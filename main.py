from fastapi import FastAPI
from pydantic import BaseModel
from joblib import load

class DataPointDTO(BaseModel):
    x: float
    y: float

class PredictionDTO(BaseModel):
    label: int
    probs: dict


app = FastAPI(title="My sklearn API",
              description="A simple sklearn API which takes input parameters and outputs classification resuts and their probabilities")

@app.on_event("startup")
def load_model():
    model = load("final_model.joblib")
    app.model = model

@app.post("/classify", response_model=PredictionDTO)
def classify(data_info: DataPointDTO):
    prediction = app.model.predict([[data_info.y, data_info.y]])[0]
    probs = dict(enumerate(app.model.predict_proba([[data_info.y, data_info.y]])[0]))

    return PredictionDTO(label=prediction, probs=probs)

